set TargetPath=simplex.exe
set SourcePath=https://github.com/simplex-chat/simplex-chat/releases/latest/download/simplex-chat-windows-x86-64

move /Y simplex.exe simplex.exe.bkp

rem powershell.exe -Command (new-object System.Net.WebClient).DownloadFile('%SourcePath%','%TargetPath%')
curl -L --ssl-no-revoke %SourcePath% --output %TargetPath%

@echo off

rem should contain path to wallet file
set config_file=%USERPROFILE%\.config\my_wallet_config.txt

rem constants:
set EMPTY='

(
    set /p wallet_file=
)<%config_file%

echo %EMPTY%
echo The password will stay on screen only, and only till the end of the script.
set /p password="Enter password >"
echo %EMPTY%

C:\Progra~1\Monero~1\monero-wallet-cli.exe ^
    --wallet-file=%wallet_file% ^
    --password=%password% ^
    %*

set password=(.)(.)
echo %EMPTY%
echo screen will be cleared after pause
pause
cls

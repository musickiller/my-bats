@echo off

REM Drop folders on this file, paste path where you wat to create a junction
REM No need to remeber the mklink command anymore!


rem %0 - this file
rem %1 - a file that was dropped on this file
rem %~n1 - file or folder name from it's full path (nx - with extension)
rem but only in FOR

set path_from=%1

echo Creating junktion link for %path_from%
echo "Please paste/enter path where the link should be created:"
set /p path_to=""

rem extracting folder name
FOR %%A IN (%path_from%) DO (
    SET folder_name=%%~nxA
)

rem @echo on
mklink /J %path_to%\%folder_name% %path_from%

pause
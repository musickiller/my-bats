[reflection.assembly]::loadwithpartialname('System.Windows.Forms')
[reflection.assembly]::loadwithpartialname('System.Drawing')
$notify = new-object system.windows.forms.notifyicon
$notify.icon = [System.Drawing.SystemIcons]::Information
$notify.visible = $true

$time_to_display_sec = 10
$message_header = 'WARNING'
$message = 'check bt status'

$notify.showballoontip(
    $time_to_display_sec,
    $message_header,
    $message,
    [system.windows.forms.tooltipicon]::None
)
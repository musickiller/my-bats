@echo off

REM Drop folders on this file, paste path where you wat to create a junction
REM No need to remeber the mklink command anymore!


rem %0 - this file
rem %1 - a file that was dropped on this file
rem %~n1 - file or folder name from it's full path (nx - with extension)
rem but only in FOR

set path_from=%1
set path_to=%~dp0%~nx1

echo Creating junktion link from
echo %path_from%
echo to
echo %path_to%

rem @echo on
mklink /D "%path_to%" %path_from%

pause
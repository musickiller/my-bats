wsl --user root service bluetooth stop

set bt_id $((usbipd list | grep Bluetooth).Substring(0, 4))
echo "found bt adapter at $bt_id, continuing..."
usbipd detach --busid=$bt_id
sleep 3
usbipd list
usbipd attach --wsl --busid=$bt_id
sleep 3
usbipd list

wsl --user root service bluetooth start
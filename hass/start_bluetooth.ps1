# set bluetooth
set bt_id $((usbipd list | grep Bluetooth).Substring(0, 4))
echo "found bt adapter at $bt_id, binding..."

$command = "usbipd bind --busid=$bt_id"
$arg_list = '-NoProfile -ExecutionPolicy Bypass -Command "$command"'
$process = Start-Process pwsh -PassThru -Wait -Verb runAs -ArgumentList "$arg_list"
if (!$process.ExitCode -eq 0) {
    echo "unable to bind adapter with usbipd"
    exit 1
}

echo "waiting before attaching..."
sleep 3
usbipd list

echo "attaching..."
try {
    usbipd attach --wsl --busid=$bt_id
}
catch {
    <#Do this if a terminating exception happens#>
    echo "usbipd is not running"
    echo $error
    exit 1
}
echo "usbipd is running with busid $bt_id"
usbipd list

echo "enabling bluetooth..."
wsl --user root bash -c "echo 'export BLUETOOTH_ENABLED=1' | tee /etc/default/bluetooth && service dbus start && service bluetooth start"
wsl lsusb
echo "checking that wsl is running"
try {
    wsl echo "WSL is running, OK."
}
catch {
    echo "wsl is not running"
    echo $error
    exit 1
}

exit 0
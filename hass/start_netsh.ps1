echo "binding addresses..."
$command = 'netsh interface portproxy add v4tov4 listenport=8123 listenaddress=192.168.1.2 connectport=8123 connectaddress=(wsl hostname -I).Replace(" ", "")'
$pwsh_arg_list = '-NoProfile -ExecutionPolicy Bypass -Command "$command"'
$process = Start-Process pwsh -PassThru -Wait -Verb runAs -ArgumentList "$pwsh_arg_list"

if (!$process.ExitCode -eq 0) {
    echo "unable to bind addresses with netsh"
    exit 1
}

echo 'successfully bound addresses with netsh'
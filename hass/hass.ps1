# imports
$parent_dir = Split-Path $MyInvocation.MyCommand.Path
# todo: add try/catch or smth
. $parent_dir\check_wsl.ps1

# start bluetooth and netsh asynchronously
try {
    $job_bt = Start-Job -FilePath $PSScriptRoot/start_bluetooth.ps1
    $job_netsh = Start-Job -FilePath $PSScriptRoot/start_netsh.ps1
} catch {
    echo "cannot start async job"
    echo $error
    exit 1
}

# Wait for the job to complete and retrieve the result
try {
    $job_netsh | Wait-Job | Receive-Job
    $job_bt | Wait-Job | Receive-Job
}
catch {
    echo "bluetooth or netsh not enabled"
    echo error
    exit 1
}


while ($true) {
    echo "starting hass after 1 second..." &&
    sleep 1 &&
    wsl --user homeassistant bash -c "source /srv/homeassistant/bin/activate && hass"

    # Trap Ctrl+C signal and execute the specified action
    trap {
        echo "Ctrl+C detected. Continuing with the rest of the script..."
        break
    }
}

echo "unbinding addresses..."
# technically, it would be better to do "portproxy delete"
sudo netsh interface portproxy reset &&
echo "done" || echo "not done"
echo "detaching usbipd..."
usbipd detach --wsl --busid=3-14 &&
echo "done" || echo "not done"
echo "unbinding usbipd..."
sudo usbipd unbind --busid=3-14 &&
echo "done" || echo "not done"
#wsl -d Ubuntu --shutdown
pause
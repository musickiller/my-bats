$time_m = 30
$time_s = $time_m * 60

while ($true) {
    echo "waiting for $time_m minutes..."
    sleep($time_s)
    Get-Date
    wsl --user root service bluetooth restart
}